//Activity

//1. STATUS: OK
//2. STATUS: OK

//3. STATUS: OK
		db.rooms.insertOne(
			{
				"name":"single",
				"accomodates":"2",
				"price":1000,
				"description": "A simple room with all the basic necessities",
				"rooms_available": 10,
				"isAvailable": false
		})


//4. STATUS: OK
		db.rooms.insertMany([
			{
				"name":"double",
				"accomodates":"3",
				"price":2000,
				"description": "A room fit for a small family going on a vacation",
				"rooms_available": 5,
				"isAvailable": false
			}, 
			{
				"name":"queen",
				"accomodates":"4",
				"price":4000,
				"description": "A room with a queen sized bed perfect for a simple getaway",
				"rooms_available": 15,
				"isAvailable": false
			}])


//5. STATUS: OK
			db.rooms.find({"name":"double"})



//6. STATUS: OK

				db.rooms.updateOne(
					   {
							"name": "queen",
						},
						{ $set: {
							"rooms_available": 0
						 } }
					   
					)
//7. STATUS: OK


				db.rooms.deleteMany(
					{ "rooms_available": 0 }
				)

//8. STATUS: OK
//9. STATUS: OK
//10. STATUS: OK





